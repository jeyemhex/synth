/*======[ OSCILLATOR ]======
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.2.1, 2019-02-08
 *
 * This code is distributed under the MIT license.
 */

#include "oscillator.h"

Oscillator *new_oscillator(int n_harmonics, float *amplitudes, Buffer *buf) {
    Oscillator *osc = malloc(sizeof(Oscillator));
    if (!osc) error_abort("Unable to allocate oscillator");
    osc->n_harmonics = n_harmonics;
    osc->amplitudes = amplitudes;
    osc->time = 0;

    if (buf->channels != 1)
        error_abort("Only single channel oscillators supported at this time");

    osc->buffer = buf;

    return osc;
}

void reset_oscillator(Oscillator *osc) {
    osc->time = 0.0;
}

void run_oscillator(Oscillator *osc, float frequency) {
    for (int i = 0; i < (osc->buffer)->frames; i++) {
        for (int n = 0; n < osc->n_harmonics; n++) {
            (osc->buffer)->next[i] += osc->amplitudes[n]
                                    * sin(2*M_PI * frequency * (1+n) * osc->time);
        }
        osc->time += 1.0 / osc->buffer->rate;
    }
}

void destroy_oscillator(Oscillator *osc) {
    free(osc);
}

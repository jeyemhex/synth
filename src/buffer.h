/*
 * buffer.h
 * Copyright (C) 2019 Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef BUFFER_H
#define BUFFER_H

#include <stdlib.h>
#include "error.h"

typedef struct {
  int rate;
  int depth;
  int channels;
  int frames;
  int synced;
  float *current;
  float *next;
} Buffer;

Buffer *new_buffer(int frames, int rate, int samples, int channels);

void copy_buffer(Buffer *buf_in, Buffer *buf_out);

void sync_buffer(Buffer *buf);

void destroy_buffer(Buffer *buf);

#endif /* !BUFFER_H */

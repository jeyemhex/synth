/*======[ BUFFER ]======
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2019-02-08
 *
 * This code is distributed under the MIT license.
 */

#include "buffer.h"

Buffer *new_buffer(int frames, int rate, int depth, int channels) {
    Buffer *buf = malloc(sizeof(Buffer));
    if (!buf) error_abort("Unable to allocate buffer");

    buf->frames = frames;
    buf->rate = rate;
    buf->depth = depth;
    if (buf->depth != 32) error_abort("Only 32bit buffers supported");
    buf->channels = channels;

    buf->current = malloc(sizeof(float) * frames * channels);
    if (!buf->current) error_abort("Unable to allocate current buffer data");

    buf->next = malloc(sizeof(float) * frames * channels);
    if (!buf->next) error_abort("Unable to allocate next buffer data");

    for (int c = 0; c < buf->channels; c++) {
        for (int i = 0; i < buf->frames; i++) {
            buf->current[buf->channels*i + c] = 0.0;
            buf->next[buf->channels*i + c] = 0.0;
        }
    }

    buf->synced = 0;

    return buf;
}

void copy_buffer(Buffer *buf_in, Buffer *buf_out) {
    if ((buf_in->frames == buf_out->frames) && (buf_in->channels == buf_out->channels)) {
        if (!buf_in->synced) error_warn("Attempting to copy unsynced buffer");
        for (int c = 0; c < buf_in->channels; c++) {
            for (int i = 0; i < buf_in->frames; i++) {
                buf_out->current[buf_in->channels*i + c] = buf_in->current[buf_in->channels*i + c];
                buf_out->next[buf_in->channels*i + c] = buf_in->next[buf_in->channels*i + c];
            }
        }
        buf_out->synced = 1;
    } else {
        error_abort("Buffers are of differet sizes");
    }
}

void sync_buffer(Buffer *buf) {
    while (buf->synced) { };
    float *tmp   = buf->current;
    buf->current = buf->next;
    buf->next    = tmp;

    buf->synced = 1;

    for (int c = 0; c < buf->channels; c++) {
        for (int i = 0; i < buf->frames; i++) {
            buf->next[buf->channels*i + c] = 0.0;
        }
    }
}

void destroy_buffer(Buffer *buf) {
    free(buf->current);
    free(buf->next);
    free(buf);
}

/*======[ ERROR ]======
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2019-02-08
 *
 * This code is distributed under the MIT license.
 */

#include "error.h"

void error_abort(char *str) {
    fprintf(stderr, "ERROR: %s\n", str);
    abort();
}

void error_warn(char *str) {
    fprintf(stderr, "WARNING: %s\n", str);
}


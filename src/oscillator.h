/*
 * oscillator.h
 * Copyright (C) 2019 Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef OSCILLATOR_H
#define OSCILLATOR_H

#include <stdlib.h>
#include <math.h>
#include "buffer.h"

typedef struct {
    int n_harmonics;
    float *amplitudes;
    double time;
    Buffer *buffer;
} Oscillator;

Oscillator *new_oscillator(int n_harmonics, float *amplitudes, Buffer *buf);

void reset_oscillator(Oscillator *osc);

void run_oscillator(Oscillator *osc, float frequency);

void destroy_oscillator(Oscillator *osc);

#endif /* !OSCILLATOR_H */

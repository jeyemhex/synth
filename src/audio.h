/*
 * audio.h
 * Copyright (C) 2019 Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef AUDIO_H
#define AUDIO_H

#include "buffer.h"
#include "oscillator.h"
#include "stream.h"


#endif /* !AUDIO_H */

/*======[ STREAM ]======
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2019-02-08
 *
 * This code is distributed under the MIT license.
 */

#include "stream.h"

Stream *new_stream(Buffer *buf) {
    Stream *st = malloc(sizeof(Stream));
    if (!st) error_abort("Unable to allocate stream");

    st->buffer = buf;

    init_stream(st);

    return st;
}

void init_stream(Stream *st) {
    PaError err = paNoError;
    PaStreamParameters outputParameters;

    err = Pa_Initialize();
    if (err != paNoError) error_abort("Failed to initalise PortAudio");

    outputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
    if (outputParameters.device == paNoDevice) {
      error_abort("No default output device.\n");
    }
    outputParameters.channelCount = st->buffer->channels;       /* mono output */
    outputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
    outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;

    err = Pa_OpenStream(
              &st->stream,
              NULL, /* no input */
              &outputParameters,
              st->buffer->rate,
              st->buffer->frames,
              paClipOff,      /* we won't output out of range samples so don't bother clipping them */
              stream_callback,
              st->buffer);
    fprintf(stderr, "%d\n", err);
    if (err != paNoError) error_abort("Failed to open stream");

    Pa_SetStreamFinishedCallback( st->stream, &stream_finished);
}

int stream_callback( const void *inputBuffer, void *outputBuffer,
        unsigned long framesPerBuffer,
        const PaStreamCallbackTimeInfo* timeInfo,
        PaStreamCallbackFlags statusFlags,
        void *buffer ) {

    float *buf_out = (float*) outputBuffer;

    Buffer *buf_in = (Buffer *) buffer;

    while (!buf_in->synced) { }

    for (int i=0; i < buf_in->frames; i++) {
        for (int c=0; c < buf_in->channels; c++) {
            *buf_out++ = buf_in->current[buf_in->channels*i + c];
        }
    }
    buf_in->synced = 0;

    return paContinue;
}

void start_stream(Stream *st) {
    PaError err = Pa_StartStream(st->stream);
    fprintf(stderr, "%d\n", err);
    if (err != paNoError) error_abort("Failed to start stream");
}

void stop_stream(Stream *st) {
    PaError err = Pa_StopStream(st->stream);
    if (err != paNoError) error_abort("Failed to stop stream");
}

void stream_finished() {
    printf("Stream finished!");
}

void destroy_stream(Stream *st) {
    free(st);
}

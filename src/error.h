/*
 * error.h
 * Copyright (C) 2019 Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef ERROR_H
#define ERROR_H

#include <stdlib.h> 
#include <stdio.h> 

void error_abort(char *str);

void error_warn(char *str);

#endif /* !ERROR_H */

/*======[ SINE_TEST ]======
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2019-02-08
 *
 * This code is distributed under the MIT license.
 */

#include <time.h>
#include "audio.h"

void callback() {
    
}

int main(void) {
    Buffer *buf = new_buffer(4410, 44100, 32, 1);

    float amps[3] = {0.3, 0.2, 0.1};
    float notes[3] = {-261.63, 329.63, 392.00};

    Oscillator *osc[3];

    Stream *stream = new_stream(buf);

    for (int i=0; i<3; i++) {
        osc[i] = new_oscillator(3, amps, buf);
        run_oscillator(osc[i], notes[i]);
    }

    start_stream(stream);

    clock_t start_time = clock();
    while (1) {
        for (int i=0; i<3; i++) {
            if (notes[i] > 0) {
                run_oscillator(osc[i], notes[i]);
            }
        }
        sync_buffer(buf);
        if (clock() - start_time > 5*CLOCKS_PER_SEC) break;
    }

    stop_stream(stream);
    destroy_stream(stream);
    for (int i=0; i<3; i++) {
        destroy_oscillator(osc[i]);
    }
    destroy_buffer(buf);
}

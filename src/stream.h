/*
 * stream.h
 * Copyright (C) 2019 Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef STREAM_H
#define STREAM_H

#include <portaudio.h>
#include "buffer.h"

typedef struct {
    PaStream *stream;
    Buffer *buffer;
} Stream;


Stream *new_stream(Buffer *buf);

void init_stream(Stream *st);

void start_stream(Stream *st);

void stop_stream(Stream *st);

int stream_callback( const void *inputBuffer, void *outputBuffer,
        unsigned long framesPerBuffer,
        const PaStreamCallbackTimeInfo* timeInfo,
        PaStreamCallbackFlags statusFlags,
        void *buffer );

void stream_finished();

void destroy_stream();

#endif /* !STREAM_H */

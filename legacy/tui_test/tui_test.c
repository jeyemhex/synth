/*======[ TUI_TEST ]======
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2019-04-24
 *
 * This code is distributed under the MIT license.
 */

#include <ncurses.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct {
    int height, width, startx, starty;
    int ntabs;
    int active_tab;
    char **names;
    WINDOW **titles;
    WINDOW **bodies;
} Tabbar;

void activate_tab(Tabbar *t, int a) {
    if (t->active_tab == 0) {
        wborder(t->titles[t->active_tab],ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE, 
                        ACS_LTEE, ACS_TTEE, ACS_LTEE, ACS_BTEE);
    } else if (t->active_tab == t->ntabs-1) {
        wborder(t->titles[t->active_tab],ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE, 
                        ACS_TTEE, ACS_RTEE, ACS_BTEE, ACS_RTEE);
    } else {
        wborder(t->titles[t->active_tab],ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE, 
                        ACS_TTEE, ACS_TTEE, ACS_BTEE, ACS_BTEE);
    }
    wrefresh(t->titles[t->active_tab]);

    t->active_tab = a;
    if (t->active_tab == 0) {
        wborder(t->titles[t->active_tab],ACS_VLINE, ACS_VLINE, ACS_HLINE, ' ', 
                        ACS_LTEE, ACS_TTEE, ACS_VLINE, ACS_LLCORNER);
    } else if (t->active_tab == t->ntabs-1) {
        wborder(t->titles[t->active_tab],ACS_VLINE, ACS_VLINE, ACS_HLINE, ' ', 
                        ACS_TTEE, ACS_RTEE, ACS_LRCORNER, ACS_VLINE);
    } else {
        wborder(t->titles[t->active_tab],ACS_VLINE, ACS_VLINE, ACS_HLINE, ' ', 
                        ACS_TTEE, ACS_TTEE, ACS_LRCORNER, ACS_LLCORNER);
    }
    wrefresh(t->titles[t->active_tab]);

    redrawwin(t->bodies[t->active_tab]);
    wrefresh(t->bodies[t->active_tab]);
}

Tabbar* new_tabbar(int height, int width,
                  int starty, int startx,
                  int ntabs, char **names) {
    Tabbar *t = malloc(sizeof(Tabbar));
    t->height = height;
    t->width  = width;
    t->startx = startx;
    t->starty = starty;
    t->ntabs = ntabs;

    t->names = malloc(ntabs * sizeof(char*));
    for (int i=0; i<ntabs; i++) {
        t->names[i] = malloc(80*sizeof(char));
        strcpy(t->names[i], names[i]);
    }

    t->titles = malloc(ntabs * sizeof(WINDOW*));
    for (int i=ntabs-1; i>=0; i--) {
        t->titles[i] = newwin(3, width/ntabs+(i!=ntabs-1), starty, startx + i*width/ntabs);
        if (i == 0) {
            wborder(t->titles[i],ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE, 
                            ACS_LTEE, ACS_TTEE, ACS_LTEE, ACS_BTEE);
        } else if (i == ntabs-1) {
            wborder(t->titles[i],ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE, 
                            ACS_TTEE, ACS_RTEE, ACS_BTEE, ACS_RTEE);
        } else {
            wborder(t->titles[i],ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE,
                            ACS_TTEE, ACS_TTEE, ACS_BTEE, ACS_BTEE);
        }

        mvwaddstr(t->titles[i], 1, 2, t->names[i]);
        wrefresh(t->titles[i]);

    }
    t->bodies = malloc(ntabs * sizeof(WINDOW*));
    for (int i=ntabs-1; i>=0; i--) {
        t->bodies[i] = newwin(11, COLS-2, 5, 1);
//EJH//         box(t->bodies[i], 0, 0);
        mvwaddstr(t->bodies[i], 1, 2, t->names[i]);
    }

    t->active_tab = 0;
    activate_tab(t, 0);
    return t;
}

void draw_keyboard(WINDOW *win) {
    int r = 0;
    for (int c=0; c<60; c++) mvwaddch(win, r, c, ACS_HLINE);

    for (r=1; r<4; r++) {
        int c=0;
        for (int octaves=0; octaves<2; octaves++) {
            mvwaddch(win, r, c++, ' ');
            mvwaddch(win, r, c++, ' ');
            mvwaddch(win, r, c++, ACS_VLINE);
            mvwaddch(win, r, c++, ' ');
            mvwaddch(win, r, c++, ACS_VLINE);
            mvwaddch(win, r, c++, ' ');
            mvwaddch(win, r, c++, ACS_VLINE);
            mvwaddch(win, r, c++, ' ');
            mvwaddch(win, r, c++, ACS_VLINE);
            mvwaddch(win, r, c++, ' ');
            mvwaddch(win, r, c++, ' ');
            mvwaddch(win, r, c++, ACS_VLINE);
            mvwaddch(win, r, c++, ' ');
            mvwaddch(win, r, c++, ' ');
            mvwaddch(win, r, c++, ACS_VLINE);
            mvwaddch(win, r, c++, ' ');
            mvwaddch(win, r, c++, ACS_VLINE);
            mvwaddch(win, r, c++, ' ');
            mvwaddch(win, r, c++, ACS_VLINE);
            mvwaddch(win, r, c++, ' ');
            mvwaddch(win, r, c++, ACS_VLINE);
            mvwaddch(win, r, c++, ' ');
            mvwaddch(win, r, c++, ACS_VLINE);
            mvwaddch(win, r, c++, ' ');
            mvwaddch(win, r, c++, ACS_VLINE);
            mvwaddch(win, r, c++, ' ');
            mvwaddch(win, r, c++, ' ');
            mvwaddch(win, r, c++, ACS_VLINE);
        }
        mvwaddch(win, r, 56, ' ');
        mvwaddch(win, r, 57, ' ');
        mvwaddch(win, r, 58, ACS_VLINE);
    }

    int c=0;
    for (int octaves=0; octaves<2; octaves++) {
        mvwaddch(win, r, c++, ' ');
        mvwaddch(win, r, c++, ' ');
        mvwaddch(win, r, c++, ACS_LLCORNER);
        mvwaddch(win, r, c++, ACS_TTEE);
        mvwaddch(win, r, c++, ACS_LRCORNER);
        mvwaddch(win, r, c++, ' ');
        mvwaddch(win, r, c++, ACS_LLCORNER);
        mvwaddch(win, r, c++, ACS_TTEE);
        mvwaddch(win, r, c++, ACS_LRCORNER);
        mvwaddch(win, r, c++, ' ');
        mvwaddch(win, r, c++, ' ');
        mvwaddch(win, r, c++, ACS_VLINE);
        mvwaddch(win, r, c++, ' ');
        mvwaddch(win, r, c++, ' ');
        mvwaddch(win, r, c++, ACS_LLCORNER);
        mvwaddch(win, r, c++, ACS_TTEE);
        mvwaddch(win, r, c++, ACS_LRCORNER);
        mvwaddch(win, r, c++, ' ');
        mvwaddch(win, r, c++, ACS_LLCORNER);
        mvwaddch(win, r, c++, ACS_TTEE);
        mvwaddch(win, r, c++, ACS_LRCORNER);
        mvwaddch(win, r, c++, ' ');
        mvwaddch(win, r, c++, ACS_LLCORNER);
        mvwaddch(win, r, c++, ACS_TTEE);
        mvwaddch(win, r, c++, ACS_LRCORNER);
        mvwaddch(win, r, c++, ' ');
        mvwaddch(win, r, c++, ' ');
        mvwaddch(win, r, c++, ACS_VLINE);
    }
    mvwaddch(win, r, c++, ' ');
    mvwaddch(win, r, c++, ' ');
    mvwaddch(win, r, c++, ACS_LLCORNER);

    for (r=5; r<8; r++) {
        int c=0;
        for (int octaves=0; octaves<2; octaves++) {
            for (int key=0; key<12; key++) {
                mvwaddch(win, r, c++, ' ');
                mvwaddch(win, r, c++, ' ');
                mvwaddch(win, r, c++, ' ');
                mvwaddch(win, r, c++, ACS_VLINE);
            }
        }
    }
    wrefresh(win);

}

void draw_graph(WINDOW *win) {
    mvwaddstr(win, 1, 2, "Graph");
    wborder(win,ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE, 
                    ACS_ULCORNER, ACS_RTEE, ACS_BTEE, ACS_LRCORNER);
    wrefresh(win);
}

int main() {
    initscr();            /* Start curses mode         */
    keypad(stdscr, true);
    curs_set(0);
    box(stdscr,0,0);
    mvaddstr(1,2,"POLY_SYNTH");
    refresh();
    char *names[5] = {"Tone control", "Envelope/Trem", "Filter/OD", "Modulation", "Echo/Reverb"};
    Tabbar *tabs = new_tabbar(3, COLS, 2, 0, 5, names);

    WINDOW *kbd_win = newwin(8, 60, 16, 1);
    draw_keyboard(kbd_win);

    WINDOW *graph_win = newwin(10, 20, 15, 60);
    draw_graph(graph_win);

    int c;
    while ((c = getch())) {
        switch (c) {
            case 'q':
                goto end;
                break;
            case KEY_LEFT:
                if (tabs->active_tab != 0)
                    activate_tab(tabs, tabs->active_tab-1);
                    redrawwin(graph_win);
                    wrefresh(graph_win);
                break;
            case KEY_RIGHT:
                if (tabs->active_tab != 5)
                    activate_tab(tabs, tabs->active_tab+1);
                    redrawwin(graph_win);
                    wrefresh(graph_win);
                break;
        }
    }
end:
    endwin();            /* End curses mode          */
    return 0;
}



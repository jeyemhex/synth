/*======[ MONO_03 ]======
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2019-02-06
 *
 * This code is distributed under the MIT license.
 */
#include <portaudio.h>
#include <portmidi.h>
//EJH// #include <ncurses.h>
#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>

#define PI 3.1415926535
#define MS 44

#define SAMPLE_RATE 44100
#define BUF_SIZE 10*MS

// Tone properties, all fairly arbitrary
#define TONE_DECAY       3      // How fast the note decays (bigger = quicker)
#define TONE_AMP         1      // Amplitude of the initial transient
#define PERCUSSION_DECAY 12     // How fast initial transient decays (bigger = quicker)
#define PERCUSSION_AMP   2      // Amplitude of the initial transient
#define PERCUSSION_HARMONIC 2   // Which harmonic to use for the initial attack
#define N_HARMONICS 3

// Which octave for the keyboards
#define UPPER_OCTAVE 4  // Upper keyboard ('t' -> ']')
#define LOWER_OCTAVE 3  // Lower keyboard ('\' -> 'm')

// Final volume 
#define PREAMP_VOL 50 // Before clipping
#define MASTER_VOL 100 // After clipping

#define USE_DELAY 0
#define DELAY_TIME (450*MS)
#define DELAY_DECAY 0.4

// Sequencer 
#define USE_SEQ 0   // use sequencer or keys
#define SEQ_BPM 165  // sequencer speed (in bpm)

typedef struct {
    int id;
    float frequency;
    float pressure;
    float time;
    bool pressed;
} Key;

typedef struct {
    Key keys[120];
    float delay_buf[DELAY_TIME];
    int   delay_loc;
    double t;
} Data;

typedef struct {
    int id;
    PortMidiStream *mstream;
} MidiInput;

// Define the sequence (in terms of physical keys, not notes; '\' needs to be escaped)
char sequence[8] = "zcvcytyu";
int seq_pos = 0;

void midi_init(MidiInput *input) {
    Pm_Initialize();
    int count = Pm_CountDevices();
    printf("Count = %d\n", count);
    for (int i=0; i < count; i++) {
        const PmDeviceInfo *info = Pm_GetDeviceInfo(i);
        if (info->input) {
            printf("%d: %s \n",  i, info->name);
        }
    }
    printf("Choose device:\t");
    scanf("%d", &input->id);

    Pm_OpenInput(&input->mstream, input->id, NULL, 512L, NULL, NULL);
}

int paCallback( const void *inputBuffer, void *outputBuffer,
                            unsigned long framesPerBuffer,
                            const PaStreamCallbackTimeInfo* timeInfo,
                            PaStreamCallbackFlags statusFlags,
                            void *data ) {
    float *buf = (float*) outputBuffer;
    Data *d = (Data *) data;
    Key *keys = d->keys;

    float f1;
    float amp; 
    float b;

    // Amplitudes of the harmonics
    float bar[N_HARMONICS];
    for (int i=0; i<N_HARMONICS; i++) {
        if (i % 2 == 0) {
            bar[i] = 1./(2*i + 2);
        } else {
            bar[i] = 0;
        }
    }

    // Relative frequencies of the harmonics
    float harmonic[N_HARMONICS];
    for (int i=0; i<N_HARMONICS; i++) {
        harmonic[i] = (1 + i);
    }

    // Generate note
    // Initial percussive/transient tone
    for (int i=0; i<BUF_SIZE; i++) {
        b = 0;
        for (int k=0; k<120; k++) {
            if (!keys[k].pressed) continue;
            amp = keys[k].pressure / 127.;
            f1 = keys[k].frequency;
            b += amp * PERCUSSION_AMP * sin(keys[k].time * 2*PI* f1 * harmonic[2] ) * exp(-PERCUSSION_DECAY*keys[k].time);

            // Treset of the tone
            for (int i=0; i<N_HARMONICS; i++) {
                b += amp * TONE_AMP * bar[i] * sin(2*PI * f1 * harmonic[i] * keys[k].time) *exp(-TONE_DECAY*keys[k].time);
            }
            keys[k].time += 1./SAMPLE_RATE;
        }
            // Apply sigmoid clipping
        b = atan(b * PREAMP_VOL / 10.) *  MASTER_VOL / 1000.;

        if (USE_DELAY) {
            d->delay_buf[d->delay_loc] = b + DELAY_DECAY * d->delay_buf[d->delay_loc];
            b = d->delay_buf[d->delay_loc];
            d->delay_loc = (d->delay_loc + 1) % DELAY_TIME;
        } 
        *buf++ = b;

//EJH//         buf++;
    }

    return paContinue;
}

void StreamFinished() {
    printf("See you again soon!");
}

void get_keys(Key *keys, MidiInput *input) {
    PmEvent msg[32];
    int i;
    if (Pm_Poll(input->mstream)) {
        Pm_Read(input->mstream, msg, 32);
        int status = Pm_MessageStatus(msg[0].message);
        if (status == 144) {
            i = Pm_MessageData1(msg[0].message);
            printf("Key %d down\n", i);
            keys[i].pressure = Pm_MessageData2(msg[0].message);
            keys[i].time = 0.0;
            keys[i].pressed = true;
        } else if (status == 128) {
            i = Pm_MessageData1(msg[0].message);
            printf("Key %d up\n", i);
            keys[i].pressed = false;
        }
    }
}

int main() {
    Data data;

    PaStreamParameters outputParameters;
    PaStream *stream;
    PaError err = paNoError;


    for (int i=0; i<120; i++) {
        data.keys[i].frequency = 440. *  pow(2, (i-57.) / 12.);
        data.keys[i].pressure = 0.0;
        data.keys[i].time = 0.0;
        data.keys[i].pressed = false;
    }

    Pa_Initialize();

    outputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
    if (outputParameters.device == paNoDevice) {
      fprintf(stderr,"Error: No default output device.\n");
      return 1;
    }
    outputParameters.channelCount = 1;       /* mono output */
    outputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
    outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;

    Pa_OpenStream(
              &stream,
              NULL, /* no input */
              &outputParameters,
              SAMPLE_RATE,
              BUF_SIZE,
              paClipOff,      /* we won't output out of range samples so don't bother clipping them */
              paCallback,
              &data);
    if( err != paNoError ) return 1 ;

    Pa_SetStreamFinishedCallback( stream, &StreamFinished );

    MidiInput input;
    midi_init(&input);

    data.delay_loc = 0;
    for (int i=0;i<DELAY_TIME;i++) {
        data.delay_buf[i] = 0.0;
    }

    Pa_StartStream( stream );
    if( err != paNoError ) return 1;

    while(1) {
        get_keys(data.keys, &input);
//EJH//         refresh();
    }

    Pa_StopStream( stream );
    if( err != paNoError ) return 1;

    Pa_CloseStream( stream );
    if( err != paNoError ) return 1;

    Pa_Terminate();
    printf("Test finished.\n");
    
    return 0;
}

/*======[ MONO_03 ]======
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2019-02-06
 *
 * This code is distributed under the MIT license.
 */
#include <portaudio.h>
#include <portmidi.h>
//EJH// #include <ncurses.h>
#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>

#define PI 3.1415926535
#define MS 44
#define SAMPLE_RATE 44100
#define BUF_SIZE 10*MS

// Tone properties, all fairly arbitrary
#define TONE_DECAY       3      // How fast the note decays (bigger = quicker)
#define TONE_AMP         1      // Amplitude of the initial transient
#define PERCUSSION_DECAY 12     // How fast initial transient decays (bigger = quicker)
#define PERCUSSION_AMP   2      // Amplitude of the initial transient
#define PERCUSSION_HARMONIC 2   // Which harmonic to use for the initial attack

// Which octave for the keyboards
#define UPPER_OCTAVE 4  // Upper keyboard ('t' -> ']')
#define LOWER_OCTAVE 3  // Lower keyboard ('\' -> 'm')

// Final volume 
#define MASTER_VOL 100 // After clipping

#define USE_DELAY 0
#define DELAY_TIME (450*MS)
#define DELAY_DECAY 0.4

// Sequencer 
#define USE_SEQ 0   // use sequencer or keys
#define SEQ_BPM 165  // sequencer speed (in bpm)

typedef struct {
    int id;
    double frequency;
    double pressure;
    double time;
    double release_time;
    double envelope_amplitude;
    bool decayed;
    bool pressed;
} Key;

typedef struct {
    double attack;
    double decay;
    double sustain;
    double release;
} Envelope;

typedef struct {
    double order;
    double amplitude;
} Harmonic;


typedef struct {
    double depth;
    double rate;
    double time;
} Tremelo;

typedef struct {
    Key keys[120];
    Envelope envelope;
    Tremelo tremelo;
    Harmonic harmonics[8];
    double delay_buf[DELAY_TIME];
    int   delay_loc;
    int sustain;
    double t;
    double gain;
} Data;

typedef struct {
    int id;
    PortMidiStream *mstream;
} MidiInput;

// Define the sequence (in terms of physical keys, not notes; '\' needs to be escaped)
char sequence[8] = "zcvcytyu";
int seq_pos = 0;

void midi_init(MidiInput *keys_input, MidiInput *knob_input) {
    Pm_Initialize();
    int count = Pm_CountDevices();
    printf("Count = %d\n", count);
    for (int i=0; i < count; i++) {
        const PmDeviceInfo *info = Pm_GetDeviceInfo(i);
        if (info->input) {
            printf("%d: %s \n",  i, info->name);
        }
    }
    printf("Choose keys device:\t");
    scanf("%d", &keys_input->id);

    Pm_OpenInput(&keys_input->mstream, keys_input->id, NULL, 512L, NULL, NULL);

    printf("Choose knob device (0 if the same):\t");
    scanf("%d", &knob_input->id);

    Pm_OpenInput(&knob_input->mstream, knob_input->id, NULL, 512L, NULL, NULL);
}

int paCallback( const void *inputBuffer, void *outputBuffer,
                            unsigned long framesPerBuffer,
                            const PaStreamCallbackTimeInfo* timeInfo,
                            PaStreamCallbackFlags statusFlags,
                            void *data ) {
    float *buf = (float*) outputBuffer;
    Data *d = (Data *) data;
    Key *keys = d->keys;

    double f1;
    double amp; 
    double b;

    // Generate note
    // Initial percussive/transient tone
    for (int i=0; i<BUF_SIZE; i++) {
        b = 0;
        for (int k=0; k<120; k++) {
            if (keys[k].decayed) continue;
            amp = keys[k].pressure / 127.;
            f1 = keys[k].frequency;
            double t = keys[k].time;

            double A = d->envelope.attack;
            double D = d->envelope.decay;
            double S = d->envelope.sustain;
            double R = d->envelope.release;
            if (keys[k].pressed || d->sustain) {
                if (t < A) {
                    keys[k].envelope_amplitude = log(t/A + 1) / log(2);
                } else {
                    keys[k].envelope_amplitude -= 1/(.001+D) * (keys[k].envelope_amplitude - S) / SAMPLE_RATE;
                    if (keys[k].envelope_amplitude < 0.01) keys[k].decayed = true;
                }

            } else {
                keys[k].envelope_amplitude -= 1/(.001+R)*keys[k].envelope_amplitude / SAMPLE_RATE;
                if (keys[k].envelope_amplitude < 0.01) keys[k].decayed = true;
            }

            // Treset of the tone
            for (int i=0; i<8; i++) {
                b += amp * keys[k].envelope_amplitude * d->harmonics[i].amplitude * sin(2*PI * f1 * d->harmonics[i].order * t);
            }


            keys[k].time += 1./SAMPLE_RATE;
        }
            // Apply sigmoid clipping
        b = atan(b * d->gain / 10.) *  MASTER_VOL / 1000.;

        b = b * (1+d->tremelo.depth * sin(2 * PI * d->tremelo.time / d->tremelo.rate));
        d->tremelo.time += (double) 1 /SAMPLE_RATE;

        if (USE_DELAY) {
            d->delay_buf[d->delay_loc] = b + DELAY_DECAY * d->delay_buf[d->delay_loc];
            b = d->delay_buf[d->delay_loc];
            d->delay_loc = (d->delay_loc + 1) % DELAY_TIME;
        } 
        *buf++ = (float) (b*16);

//EJH//         buf++;
    }

    return paContinue;
}

void StreamFinished() {
    printf("See you again soon!");
}

void get_inputs(Data *data, MidiInput *input) {
    PmEvent msg[32];
    int i;
    if (Pm_Poll(input->mstream)) {
        Pm_Read(input->mstream, msg, 32);
        int status = Pm_MessageStatus(msg[0].message);

        switch (status) {

            case 144:
            i = Pm_MessageData1(msg[0].message);
            if (Pm_MessageData2(msg[0].message) != 0) {
                data->keys[i].pressure = Pm_MessageData2(msg[0].message);
                printf("Key %d down\n", i);
                data->keys[i].time = 0.0;
                data->keys[i].envelope_amplitude = 1.0;
                data->keys[i].envelope_amplitude = 1.0;
                data->keys[i].release_time = 0.0;
                data->keys[i].pressed = true;
                data->keys[i].decayed = false;
                break;
            }

            case 128:
            i = Pm_MessageData1(msg[0].message);
            printf("Key %d up\n", i);
            data->keys[i].pressed = false;
            data->keys[i].release_time = data->keys[i].time;
            break;

            case 176:
            if ( Pm_MessageData1(msg[0].message) == 7)  {
                data->envelope.attack = Pm_MessageData2(msg[0].message) / 127.;
                printf("Attack set to %f\n", data->envelope.attack);

            } else if ( Pm_MessageData1(msg[0].message) == 64)  {
                data->sustain = Pm_MessageData2(msg[0].message) != 0;
                printf("Sustain set to %d\n", data->sustain);

            } else {
                int i = Pm_MessageData1(msg[0].message)-12;
                data->harmonics[i].amplitude = Pm_MessageData2(msg[0].message) / 127.;
                printf("Harmonic %d set to %f\n", i, data->harmonics[i].amplitude);
            }
            break;

            case 177:
            if ( Pm_MessageData1(msg[0].message) == 7) {
                data->envelope.decay = Pm_MessageData2(msg[0].message) / 127.;
                printf("Decay set to %f\n", data->envelope.decay);
            }
            break;
            case 178:
            if ( Pm_MessageData1(msg[0].message) == 7)  {
                data->envelope.sustain = Pm_MessageData2(msg[0].message) / 127.;
                printf("Sustain set to %f\n", data->envelope.sustain);
            }
            break;
            case 179:
            if ( Pm_MessageData1(msg[0].message) == 7)  {
                data->envelope.release = Pm_MessageData2(msg[0].message) / 127.;
                printf("Release set to %f\n", data->envelope.release);
            }
            break;

            case 180:
            if ( Pm_MessageData1(msg[0].message) == 7)  {
                data->tremelo.rate = Pm_MessageData2(msg[0].message) / 127.;
                printf("Trem. rate set to %f\n", data->tremelo.rate);
            }
            break;

            case 181:
            if ( Pm_MessageData1(msg[0].message) == 7)  {
                data->tremelo.depth = Pm_MessageData2(msg[0].message) / 127.;
                printf("Trem. depth set to %f\n", data->tremelo.depth);
            }
            break;

            case 184:
            if ( Pm_MessageData1(msg[0].message) == 7)  {
                data->gain = 50 * Pm_MessageData2(msg[0].message) / 127.;
                printf("Gain set to %f\n", data->gain);
            }
            break;
        }
    }
}

int main() {
    Data data;

    PaStreamParameters outputParameters;
    PaStream *stream;
    PaError err = paNoError;


    for (int i=0; i<120; i++) {
        data.keys[i].frequency = 440. *  pow(2, (i-69.) / 12.);
        data.keys[i].pressure = 0.0;
        data.keys[i].time = 0.0;
        data.keys[i].pressed = false;
        data.keys[i].decayed = true;
    }

    for (int i=0; i<9; i++) {
        // Amplitudes of the harmonics
        if (i == 0) {
            data.harmonics[i].amplitude = 1.0;
        } else {
            data.harmonics[i].amplitude = 0;
        }

        // Relative frequencies of the harmonics
        for (int i=0; i<8; i++) {
            data.harmonics[i].order = (1+i);
        }
        data.harmonics[8].order = 0.5;
    }

    data.envelope.attack = 0.01;
    data.envelope.decay = 1;
    data.envelope.sustain = 0.0;
    data.envelope.release = 0.1;
    
    data.tremelo.rate = 1.0;
    data.tremelo.depth = 0.5;
    data.tremelo.time = 0.0;

    data.gain = 1.0;
    data.sustain = 0;

    Pa_Initialize();

    outputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
    if (outputParameters.device == paNoDevice) {
      fprintf(stderr,"Error: No default output device.\n");
      return 1;
    }
    outputParameters.channelCount = 1;       /* mono output */
    outputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
    outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;

    Pa_OpenStream(
              &stream,
              NULL, /* no input */
              &outputParameters,
              SAMPLE_RATE,
              BUF_SIZE,
              paClipOff,      /* we won't output out of range samples so don't bother clipping them */
              paCallback,
              &data);
    if( err != paNoError ) return 1 ;

    Pa_SetStreamFinishedCallback( stream, &StreamFinished );

    MidiInput keys_input, knob_input;
    midi_init(&keys_input, &knob_input);

    data.delay_loc = 0;
    for (int i=0;i<DELAY_TIME;i++) {
        data.delay_buf[i] = 0.0;
    }

    Pa_StartStream( stream );
    if( err != paNoError ) return 1;

    while(1) {
        get_inputs(&data, &keys_input);
        if (knob_input.id) get_inputs(&data, &knob_input);
//EJH//         refresh();
    }

    Pa_StopStream( stream );
    if( err != paNoError ) return 1;

    Pa_CloseStream( stream );
    if( err != paNoError ) return 1;

    Pa_Terminate();
    printf("Test finished.\n");
    
    return 0;
}

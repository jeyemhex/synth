/*======[ MONO_03 ]======
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2019-02-06
 *
 * This code is distributed under the MIT license.
 */
#include <portaudio.h>
#include <ncurses.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>

#define PI 3.1415926535
#define MS 44

#define SAMPLE_RATE 44100
#define BUF_SIZE 10*MS

// Tone properties, all fairly arbitrary
#define TONE_DECAY       3      // How fast the note decays (bigger = quicker)
#define TONE_AMP         1      // Amplitude of the initial transient
#define PERCUSSION_DECAY 12     // How fast initial transient decays (bigger = quicker)
#define PERCUSSION_AMP   0      // Amplitude of the initial transient
#define PERCUSSION_HARMONIC 2   // Which harmonic to use for the initial attack
#define N_HARMONICS 20

// Which octave for the keyboards
#define UPPER_OCTAVE 5  // Upper keyboard ('t' -> ']')
#define LOWER_OCTAVE 4  // Lower keyboard ('\' -> 'm')

// Final volume 
#define PREAMP_VOL 20 // Before clipping
#define MASTER_VOL 100 // After clipping

#define USE_DELAY 1
#define DELAY_TIME (200*MS)
#define DELAY_DECAY 0.4

// Sequencer 
#define USE_SEQ 0   // use sequencer or keys
#define SEQ_BPM 165  // sequencer speed (in bpm)

#define minval(x,y) ((x) <= (y)) ? (x) : (y)

// Define the sequence (in terms of physical keys, not notes; '\' needs to be escaped)
char sequence[8] = "zcvcytyu";
int seq_pos = 0;

void screen_init() {
    initscr();
    cbreak();
    keypad(stdscr, TRUE);
    timeout(-1);
    nodelay(curscr, 0);
    noecho();
}

typedef struct {
    float frequency;
    float delay_buf[DELAY_TIME];
    int   delay_loc;
    double t;
} Data;

int paCallback( const void *inputBuffer, void *outputBuffer,
                            unsigned long framesPerBuffer,
                            const PaStreamCallbackTimeInfo* timeInfo,
                            PaStreamCallbackFlags statusFlags,
                            void *data ) {
    float *buf = (float*) outputBuffer;
    Data *d = (Data *) data;
    float f1 = (float) d->frequency;
    float b;

    // Amplitudes of the harmonics
    float bar[N_HARMONICS];
    for (int i=0; i<N_HARMONICS; i++) {
        bar[i] = pow(-1,i+1) * 2.25/pow(PI*(i+1),2) * sin((i+1) * PI / 3.);
//EJH//         bar[i] += 0.1 * rand()/RAND_MAX;
    }

    // Relative frequencies of the harmonics
    float harmonic[N_HARMONICS];
    for (int i=0; i<N_HARMONICS; i++) {
        harmonic[i] = (1 + i);
    }

    // Generate note
    // Initial percussive/transient tone
    for (int i=0; i<BUF_SIZE; i++) {
        b = PERCUSSION_AMP * sin(d->t * 2*PI* f1 * harmonic[2] ) * exp(-PERCUSSION_DECAY*d->t);

        // Treset of the tone
        if (d->t > 0.5) {
            for (int i=0; i<N_HARMONICS; i++) {
                b += TONE_AMP * bar[i] * sin(2*PI * f1 * harmonic[i] * d->t);
            }
        } else {
            for (int i=0; i<N_HARMONICS; i++) {
                b += TONE_AMP * bar[i] * sin(2*PI * f1 * harmonic[i] * d->t) * pow(2*d->t,2);
            }
        }

        // Apply sigmoid clipping
        b = atan(b * PREAMP_VOL / 10.) *  MASTER_VOL / 1000.;

        b *= 1+sin(2*PI*5*d->t)/10;


        if (USE_DELAY) {
            d->delay_buf[d->delay_loc] = b + DELAY_DECAY * d->delay_buf[d->delay_loc];
            b = d->delay_buf[d->delay_loc];
            d->delay_loc = (d->delay_loc + 1) % DELAY_TIME;
        } 
        *buf++ = b;

        d->t += 1./SAMPLE_RATE;
//EJH//         buf++;
    }

    return paContinue;
}

void StreamFinished() {
    printf("See you again soon!");
}

float get_freq(char key) {
    float f1;
    //
    // Get frequency from key
    switch (key) {
        case'\\': f1 = 261.63 * pow(2, LOWER_OCTAVE - 4); break; // C
        case 'a': f1 = 277.18 * pow(2, LOWER_OCTAVE - 4); break; // C#
        case 'z': f1 = 293.66 * pow(2, LOWER_OCTAVE - 4); break; // D
        case 's': f1 = 311.13 * pow(2, LOWER_OCTAVE - 4); break; // D#
        case 'x': f1 = 329.63 * pow(2, LOWER_OCTAVE - 4); break; // E
        case 'c': f1 = 349.23 * pow(2, LOWER_OCTAVE - 4); break; // F
        case 'f': f1 = 369.99 * pow(2, LOWER_OCTAVE - 4); break; // F#
        case 'v': f1 = 392.00 * pow(2, LOWER_OCTAVE - 4); break; // G
        case 'g': f1 = 415.30 * pow(2, LOWER_OCTAVE - 4); break; // G#
        case 'b': f1 = 440.00 * pow(2, LOWER_OCTAVE - 4); break; // A
        case 'h': f1 = 466.16 * pow(2, LOWER_OCTAVE - 4); break; // A#
        case 'n': f1 = 493.88 * pow(2, LOWER_OCTAVE - 4); break; // B
        case 'm': f1 = 523.25 * pow(2, LOWER_OCTAVE - 4); break; // C

        case 't': f1 = 523.25 * pow(2, UPPER_OCTAVE - 4)/2; break; // C
        case '6': f1 = 554.37 * pow(2, UPPER_OCTAVE - 4)/2; break; // C#
        case 'y': f1 = 587.33 * pow(2, UPPER_OCTAVE - 4)/2; break; // D
        case '7': f1 = 622.25 * pow(2, UPPER_OCTAVE - 4)/2; break; // D#
        case 'u': f1 = 659.25 * pow(2, UPPER_OCTAVE - 4)/2; break; // E
        case 'i': f1 = 698.46 * pow(2, UPPER_OCTAVE - 4)/2; break; // F
        case '9': f1 = 739.99 * pow(2, UPPER_OCTAVE - 4)/2; break; // F#
        case 'o': f1 = 783.99 * pow(2, UPPER_OCTAVE - 4)/2; break; // G
        case '0': f1 = 830.61 * pow(2, UPPER_OCTAVE - 4)/2; break; // G#
        case 'p': f1 = 880.00 * pow(2, UPPER_OCTAVE - 4)/2; break; // A
        case '-': f1 = 932.33 * pow(2, UPPER_OCTAVE - 4)/2; break; // A#
        case '[': f1 = 987.77 * pow(2, UPPER_OCTAVE - 4)/2; break; // B
        case ']': f1 =1046.50 * pow(2, UPPER_OCTAVE - 4)/2; break; // C

        default: f1 = 0;
    }

    return f1;
}

int main() {
    Data data;

    PaStreamParameters outputParameters;
    PaStream *stream;
    PaError err = paNoError;

    Pa_Initialize();

    outputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
    if (outputParameters.device == paNoDevice) {
      fprintf(stderr,"Error: No default output device.\n");
      return 1;
    }
    outputParameters.channelCount = 1;       /* mono output */
    outputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
    outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;

    Pa_OpenStream(
              &stream,
              NULL, /* no input */
              &outputParameters,
              SAMPLE_RATE,
              BUF_SIZE,
              paClipOff,      /* we won't output out of range samples so don't bother clipping them */
              paCallback,
              &data);
    if( err != paNoError ) return 1 ;

    Pa_SetStreamFinishedCallback( stream, &StreamFinished );

    screen_init();

    data.delay_loc = 0;
    for (int i=0;i<DELAY_TIME;i++) {
        data.delay_buf[i] = 0.0;
    }

    Pa_StartStream( stream );
    if( err != paNoError ) return 1;

    char key;
    while(1) {
        if (USE_SEQ) {
            key = sequence[seq_pos++ % 8];
            usleep(1. / (SEQ_BPM / 60.) * 1000 * 1000 / 4 );

        // Else, get a key from the keyboard
        } else {
            key = getch();
        }


        data.frequency = get_freq(key);

        if (key == 'q') break;
        data.t = 0;
    }

    Pa_StopStream( stream );
    if( err != paNoError ) return 1;

    Pa_CloseStream( stream );
    if( err != paNoError ) return 1;

    Pa_Terminate();
    printf("Test finished.\n");
    
    endwin();

    return 0;
}

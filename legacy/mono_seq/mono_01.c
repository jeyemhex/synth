/*======[ TONE ]======
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2019-02-04
 *
 * This code is distributed under the MIT license.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <curses.h>
#include <pthread.h>
#include <unistd.h>

#define PI 3.1415926535
#define MS 44

// Audio sampling properties, it might break if you change these
#define SAMPLE_RATE 44100
#define BUFF_SIZE 10*MS

// Tone properties, all fairly arbitrary
#define TONE_DECAY       2      // How fast the note decays (bigger = quicker)
#define PERCUSSION_DECAY 12     // How fast initial transient decays (bigger = quicker)
#define PERCUSSION_AMP   200    // Amplitude of the initial transient
#define PERCUSSION_HARMONIC 3   // Which harmonic to use for the initial attack

// Which octave for the keyboards
#define UPPER_OCTAVE 4  // Upper keyboard ('t' -> ']')
#define LOWER_OCTAVE 3  // Lower keyboard ('\' -> 'm')

// Final volume 
#define PREAMP_VOL 100 // Before clipping
#define MASTER_VOL 100 // After clipping

// Sequencer 
#define USE_SEQ 1   // use sequencer or keys
#define SEQ_BPM 90  // sequencer speed (in bpm)


// Initialise ncurses, make sure key presses are non-blocking etc...
void screen_init() {
    initscr();
    cbreak();
    keypad(stdscr, TRUE);
    timeout(-1);
    nodelay(curscr, 0);
    noecho();
}

// Play a note at the frequency in f_ptr; percussion triggered on change of note
void *play_note(void *f_ptr) {
    double t = 0;
    float *f1 = (float *) f_ptr;
    float prev_f = 0;
    int16_t buff[BUFF_SIZE] = {0};

    // Open pipe to the aplay cmd to allow us to play the note
    FILE *pipeout;
    pipeout = popen("play -t raw -r 44100 -b 16 -e signed -c 1 - &>/dev/null", "w");

    // Amplitudes of the harmonics
    int bar[9] = {100, 80, 60, 40,   0,   0,   0,   0,   0};

    // Relative frequencies of the harmonics
    float harmonic[9] = {0.500000, 1.000000, 1.498824,
                         2.000000, 2.997647, 4.000000,
                         5.040941, 5.995294, 8.000000};

    while(1) {

        // If frequency of note has changed, reset time
        if (prev_f != *f1) t = 0;
        prev_f = *f1;

        // Generate note
        for (int i=0; i<BUFF_SIZE; i++) {
            // Initial percussive/transient tone
            buff[i] = PERCUSSION_AMP * sin(t * 2*PI* (*f1) * harmonic[3] ) * exp(-PERCUSSION_DECAY*t);

            // The reset of the tone
            buff[i] += bar[0] * sin(2*PI * (*f1) * harmonic[0] * t) *exp(-TONE_DECAY*t);
            buff[i] += bar[1] * sin(2*PI * (*f1) * harmonic[1] * t) *exp(-TONE_DECAY*t);
            buff[i] += bar[2] * sin(2*PI * (*f1) * harmonic[2] * t) *exp(-TONE_DECAY*t);
            buff[i] += bar[3] * sin(2*PI * (*f1) * harmonic[3] * t) *exp(-TONE_DECAY*t);
            buff[i] += bar[4] * sin(2*PI * (*f1) * harmonic[4] * t) *exp(-TONE_DECAY*t);
            buff[i] += bar[5] * sin(2*PI * (*f1) * harmonic[5] * t) *exp(-TONE_DECAY*t);
            buff[i] += bar[6] * sin(2*PI * (*f1) * harmonic[6] * t) *exp(-TONE_DECAY*t);
            buff[i] += bar[7] * sin(2*PI * (*f1) * harmonic[7] * t) *exp(-TONE_DECAY*t);
            buff[i] += bar[8] * sin(2*PI * (*f1) * harmonic[8] * t) *exp(-TONE_DECAY*t);

            // Advance the time by 1 sample
            t += 1.0/SAMPLE_RATE;
        }

        // Apply sigmoid clipping
        for (int i=0; i<BUFF_SIZE; i++) {
            buff[i] = 1. / (1 + exp(-buff[i] * PREAMP_VOL / 5000.)) * 100* MASTER_VOL;
        }

        // Write the tone to aplay
        fwrite(buff, 2, BUFF_SIZE, pipeout);
    }

    pclose(pipeout);
}


int main() {
    screen_init();
    float f1=0;

    // Spawn thread to generate/play note at frequency in f1 
    pthread_t play_note_thread;
    if(pthread_create(&play_note_thread, NULL, play_note, &f1)) {
        printf("Error creating thread");
        return 1;
    }

    // Define the sequence (in terms of physical keys, not notes; '\' needs to be escaped)
    char sequence[8] = "\\svm7o]]";
    int seq_pos = 0;

    char key;
    // Main loop
    while(1) {
        // If using the sequencer, get the next note and wait to play it
        if (USE_SEQ) {
            key = sequence[seq_pos++ % 8];
            usleep(1. / (SEQ_BPM / 60.) * 1000 * 1000 / 4 );

        // Else, get a key from the keyboard
        } else {
            key = getch();
        }

        // Exit if 'q' is pressed
        if (key == 'q') break;

        // Get frequency from key
        switch (key) {
            case'\\': f1 = 261.63 * pow(2, LOWER_OCTAVE - 4); break; // C
            case 'a': f1 = 277.18 * pow(2, LOWER_OCTAVE - 4); break; // C#
            case 'z': f1 = 293.66 * pow(2, LOWER_OCTAVE - 4); break; // D
            case 's': f1 = 311.13 * pow(2, LOWER_OCTAVE - 4); break; // D#
            case 'x': f1 = 329.63 * pow(2, LOWER_OCTAVE - 4); break; // E
            case 'c': f1 = 349.23 * pow(2, LOWER_OCTAVE - 4); break; // F
            case 'f': f1 = 369.99 * pow(2, LOWER_OCTAVE - 4); break; // F#
            case 'v': f1 = 392.00 * pow(2, LOWER_OCTAVE - 4); break; // G
            case 'g': f1 = 415.30 * pow(2, LOWER_OCTAVE - 4); break; // G#
            case 'b': f1 = 440.00 * pow(2, LOWER_OCTAVE - 4); break; // A
            case 'h': f1 = 466.16 * pow(2, LOWER_OCTAVE - 4); break; // A#
            case 'n': f1 = 493.88 * pow(2, LOWER_OCTAVE - 4); break; // B
            case 'm': f1 = 523.25 * pow(2, LOWER_OCTAVE - 4); break; // C

            case 't': f1 = 523.25 * pow(2, UPPER_OCTAVE - 4)/2; break; // C
            case '6': f1 = 554.37 * pow(2, UPPER_OCTAVE - 4)/2; break; // C#
            case 'y': f1 = 587.33 * pow(2, UPPER_OCTAVE - 4)/2; break; // D
            case '7': f1 = 622.25 * pow(2, UPPER_OCTAVE - 4)/2; break; // D#
            case 'u': f1 = 659.25 * pow(2, UPPER_OCTAVE - 4)/2; break; // E
            case 'i': f1 = 698.46 * pow(2, UPPER_OCTAVE - 4)/2; break; // F
            case '9': f1 = 739.99 * pow(2, UPPER_OCTAVE - 4)/2; break; // F#
            case 'o': f1 = 783.99 * pow(2, UPPER_OCTAVE - 4)/2; break; // G
            case '0': f1 = 830.61 * pow(2, UPPER_OCTAVE - 4)/2; break; // G#
            case 'p': f1 = 880.00 * pow(2, UPPER_OCTAVE - 4)/2; break; // A
            case '-': f1 = 932.33 * pow(2, UPPER_OCTAVE - 4)/2; break; // A#
            case '[': f1 = 987.77 * pow(2, UPPER_OCTAVE - 4)/2; break; // B
            case ']': f1 =1046.50 * pow(2, UPPER_OCTAVE - 4)/2; break; // C

            default: f1 = 0;
        }

        // Print something nice so the screen's not blank
        mvprintw(1,1, "Key = %c, freq = %f", key, f1);

        refresh();
    }

    // End the ncurses session
    endwin();
    return 0;
}


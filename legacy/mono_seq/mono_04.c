/*======[ MONO_03 ]======
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2019-02-06
 *
 * This code is distributed under the MIT license.
 */
#include <portaudio.h>
#include <portmidi.h>
#include <ncurses.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>

#define PI 3.1415926535
#define MS 44

#define SAMPLE_RATE 44100
#define BUF_SIZE 10*MS

// Tone properties, all fairly arbitrary
#define TONE_DECAY       3      // How fast the note decays (bigger = quicker)
#define TONE_AMP         1      // Amplitude of the initial transient
#define PERCUSSION_DECAY 12     // How fast initial transient decays (bigger = quicker)
#define PERCUSSION_AMP   2      // Amplitude of the initial transient
#define PERCUSSION_HARMONIC 2   // Which harmonic to use for the initial attack
#define N_HARMONICS 3

// Which octave for the keyboards
#define UPPER_OCTAVE 4  // Upper keyboard ('t' -> ']')
#define LOWER_OCTAVE 3  // Lower keyboard ('\' -> 'm')

// Final volume 
#define PREAMP_VOL 50 // Before clipping
#define MASTER_VOL 100 // After clipping

#define USE_DELAY 1
#define DELAY_TIME (450*MS)
#define DELAY_DECAY 0.4

// Sequencer 
#define USE_SEQ 0   // use sequencer or keys
#define SEQ_BPM 165  // sequencer speed (in bpm)

typedef struct {
    int id;
    float frequency;
    float pressure;
} Key;

typedef struct {
    Key key;
    float delay_buf[DELAY_TIME];
    int   delay_loc;
    double t;
} Data;

typedef struct {
    int id;
    PortMidiStream *mstream;
} MidiInput;

// Define the sequence (in terms of physical keys, not notes; '\' needs to be escaped)
char sequence[8] = "zcvcytyu";
int seq_pos = 0;

void screen_init() {
    initscr();
    cbreak();
    keypad(stdscr, TRUE);
    timeout(-1);
    nodelay(curscr, 0);
    noecho();
}

void midi_init(MidiInput *input) {
    Pm_Initialize();
    int count = Pm_CountDevices();
    printf("Count = %d\n", count);
    for (int i=0; i < count; i++) {
        const PmDeviceInfo *info = Pm_GetDeviceInfo(i);
        if (info->input) {
            printf("%d: %s \n",  i, info->name);
        }
    }
    printf("Choose device:\t");
    scanf("%d", &input->id);

    Pm_OpenInput(&input->mstream, input->id, NULL, 512L, NULL, NULL);
}

int paCallback( const void *inputBuffer, void *outputBuffer,
                            unsigned long framesPerBuffer,
                            const PaStreamCallbackTimeInfo* timeInfo,
                            PaStreamCallbackFlags statusFlags,
                            void *data ) {
    float *buf = (float*) outputBuffer;
    Data *d = (Data *) data;
    Key key = d->key;
    float f1 = (float) key.frequency;
    float amp = key.pressure / 127.; 
    float b;

    // Amplitudes of the harmonics
    float bar[N_HARMONICS];
    for (int i=0; i<N_HARMONICS; i++) {
        if (i % 2 == 0) {
            bar[i] = 1./(2*i + 2);
        } else {
            bar[i] = 0;
        }
    }

    // Relative frequencies of the harmonics
    float harmonic[N_HARMONICS];
    for (int i=0; i<N_HARMONICS; i++) {
        harmonic[i] = (1 + i);
    }

    // Generate note
    // Initial percussive/transient tone
    for (int i=0; i<BUF_SIZE; i++) {
        b = amp * PERCUSSION_AMP * sin(d->t * 2*PI* f1 * harmonic[2] ) * exp(-PERCUSSION_DECAY*d->t);

        // Treset of the tone
        for (int i=0; i<N_HARMONICS; i++) {
            b += amp * TONE_AMP * bar[i] * sin(2*PI * f1 * harmonic[i] * d->t) *exp(-TONE_DECAY*d->t);
        }
        // Apply sigmoid clipping
        b = atan(b * PREAMP_VOL / 10.) *  MASTER_VOL / 1000.;

        if (USE_DELAY) {
            d->delay_buf[d->delay_loc] = b + DELAY_DECAY * d->delay_buf[d->delay_loc];
            b = d->delay_buf[d->delay_loc];
            d->delay_loc = (d->delay_loc + 1) % DELAY_TIME;
        } 
        *buf++ = b;

        d->t += 1./SAMPLE_RATE;
//EJH//         buf++;
    }

    return paContinue;
}

void StreamFinished() {
    printf("See you again soon!");
}

void get_key(Key *key, MidiInput *input) {
    PmEvent msg[32];
    if (Pm_Poll(input->mstream)) {
        Pm_Read(input->mstream, msg, 32);
        if (Pm_MessageStatus(msg[0].message) != 144) return;
        key->id       = Pm_MessageData1(msg[0].message);
        key->pressure = Pm_MessageData2(msg[0].message);

        if (key->pressure > 0) {
            mvprintw(1,1,"Read key %d, pressure %f", key->id, key->pressure);
            switch (key->id) {
                case 48: key->frequency = 261.63 * pow(2, LOWER_OCTAVE - 4); break; // C
                case 49: key->frequency = 277.18 * pow(2, LOWER_OCTAVE - 4); break; // C#
                case 50: key->frequency = 293.66 * pow(2, LOWER_OCTAVE - 4); break; // D
                case 51: key->frequency = 311.13 * pow(2, LOWER_OCTAVE - 4); break; // D#
                case 52: key->frequency = 329.63 * pow(2, LOWER_OCTAVE - 4); break; // E
                case 53: key->frequency = 349.23 * pow(2, LOWER_OCTAVE - 4); break; // F
                case 54: key->frequency = 369.99 * pow(2, LOWER_OCTAVE - 4); break; // F#
                case 55: key->frequency = 392.00 * pow(2, LOWER_OCTAVE - 4); break; // G
                case 56: key->frequency = 415.30 * pow(2, LOWER_OCTAVE - 4); break; // G#
                case 57: key->frequency = 440.00 * pow(2, LOWER_OCTAVE - 4); break; // A
                case 58: key->frequency = 466.16 * pow(2, LOWER_OCTAVE - 4); break; // A#
                case 59: key->frequency = 493.88 * pow(2, LOWER_OCTAVE - 4); break; // B
                case 60: key->frequency = 523.25 * pow(2, LOWER_OCTAVE - 4); break; // C

                case 61: key->frequency = 554.37 * pow(2, UPPER_OCTAVE - 4)/2; break; // C#
                case 62: key->frequency = 587.33 * pow(2, UPPER_OCTAVE - 4)/2; break; // D
                case 63: key->frequency = 622.25 * pow(2, UPPER_OCTAVE - 4)/2; break; // D#
                case 64: key->frequency = 659.25 * pow(2, UPPER_OCTAVE - 4)/2; break; // E
                case 65: key->frequency = 698.46 * pow(2, UPPER_OCTAVE - 4)/2; break; // F
                case 66: key->frequency = 739.99 * pow(2, UPPER_OCTAVE - 4)/2; break; // F#
                case 67: key->frequency = 783.99 * pow(2, UPPER_OCTAVE - 4)/2; break; // G
                case 68: key->frequency = 830.61 * pow(2, UPPER_OCTAVE - 4)/2; break; // G#
                case 69: key->frequency = 880.00 * pow(2, UPPER_OCTAVE - 4)/2; break; // A
                case 70: key->frequency = 932.33 * pow(2, UPPER_OCTAVE - 4)/2; break; // A#
                case 71: key->frequency = 987.77 * pow(2, UPPER_OCTAVE - 4)/2; break; // B
                case 72: key->frequency =1046.50 * pow(2, UPPER_OCTAVE - 4)/2; break; // C
                default: key->frequency = 0.0; break;
            }
        }
    }
}

int main() {
    Data data;

    PaStreamParameters outputParameters;
    PaStream *stream;
    PaError err = paNoError;

    Pa_Initialize();

    outputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
    if (outputParameters.device == paNoDevice) {
      fprintf(stderr,"Error: No default output device.\n");
      return 1;
    }
    outputParameters.channelCount = 1;       /* mono output */
    outputParameters.sampleFormat = paFloat32; /* 32 bit floating point output */
    outputParameters.suggestedLatency = Pa_GetDeviceInfo( outputParameters.device )->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;

    Pa_OpenStream(
              &stream,
              NULL, /* no input */
              &outputParameters,
              SAMPLE_RATE,
              BUF_SIZE,
              paClipOff,      /* we won't output out of range samples so don't bother clipping them */
              paCallback,
              &data);
    if( err != paNoError ) return 1 ;

    Pa_SetStreamFinishedCallback( stream, &StreamFinished );

    MidiInput input;
    midi_init(&input);

    screen_init();

    data.delay_loc = 0;
    for (int i=0;i<DELAY_TIME;i++) {
        data.delay_buf[i] = 0.0;
    }

    Pa_StartStream( stream );
    if( err != paNoError ) return 1;

    Key key;
    key.frequency = 0;
    key.id = 0;
    key.pressure = 0;

    while(1) {
        get_key(&key, &input);
        if (key.frequency != data.key.frequency
        || (key.pressure != data.key.pressure && key.pressure != 0)) {
            mvprintw(2,1,"Playing frequency %f, amplitude %f", key.frequency, key.pressure);
            data.t = 0;
        }
        if (key.pressure > 0) {
            data.key.frequency = key.frequency;
            data.key.pressure = key.pressure;
        }

        refresh();
    }

    Pa_StopStream( stream );
    if( err != paNoError ) return 1;

    Pa_CloseStream( stream );
    if( err != paNoError ) return 1;

    Pa_Terminate();
    printf("Test finished.\n");
    
    return 0;
}

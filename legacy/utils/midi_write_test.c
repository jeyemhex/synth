/*======[ MIDI_WRITE_TEST ]======
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2019-04-20
 *
 * This code is distributed under the MIT license.
 */

#include <stdio.h>
#include<portmidi.h>
#include<porttime.h>

int main() {
    
    int cnt, i, dev;
    PmError retval;
    const PmDeviceInfo *info;
    PortMidiStream *mstream;

    Pm_Initialize();
    cnt = Pm_CountDevices();

    if (cnt) {
        for (i=0; i<cnt; i++) {
            info = Pm_GetDeviceInfo(i);
            if (info->output) printf("%d:%s \n", i, info->name);
        }

        printf("Choose device: ");
        scanf("%d", &dev);

        Pt_Start(1, NULL, NULL);
        retval = Pm_OpenOutput(&mstream, dev, NULL, 512L, NULL, NULL, 0);
        if (retval != pmNoError) {
            printf("error: %s\n", Pm_GetErrorText(retval));

        } else {
            while (1) {
                for (int control=64; control < 72 ;control++){
                    printf("Writing to control %d\n", control);
                    for (int value = 0; value < 128; value++) {
                        long time = Pt_Time(NULL);
                        Pm_WriteShort(mstream, 0, Pm_Message(176,  control, value));
                        printf("  writing %d\t%d\t%d\n", 176,  control, value);
                        while(Pt_Time(NULL) - time < 10);
                    }
                    Pm_WriteShort(mstream, 0, Pm_Message(176, control, 0));
                }
                int control = 80;
                printf("Writing to control %d\n",  control);
                for (int value = 0; value < 128; value++) {
                    long time = Pt_Time(NULL);
                    Pm_WriteShort(mstream, 0, Pm_Message(176, control, value));
                    printf("  writing %d\t%d\t%d\n", 176, control, value);
                    while(Pt_Time(NULL) - time < 10);
                    Pm_WriteShort(mstream, 0, Pm_Message(176, control, 0));
                }


                for (int control=112; control < 128 ;control++){
                    printf("Writing to control %d\n", control);
                    long time = Pt_Time(NULL);
                    Pm_WriteShort(mstream, 0, Pm_Message(176, control, 127));
                    printf("  writing %d\t%d\t%d\n", 176, control, 127);
                    while(Pt_Time(NULL) - time < 100);
                    Pm_WriteShort(mstream, 0, Pm_Message(176, control, 0));
                    printf("  writing %d\t%d\t%d\n", 176, control, 0);
                }
                return 0;
            }
        }
        Pm_Close(mstream);
    }

    else printf("No MIDI device found\n");
    Pm_Terminate();
    return 0;
}

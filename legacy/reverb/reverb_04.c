//
// readwav.c - Read samples from a WAV file using FFmpeg via a pipe
// Written by Ted Burke - last updated 10-2-2017
//
// To compile:
//
//    gcc readwav.c -o readwav -lm
//
 
#include <stdio.h>
#include <stdint.h>
 
#define MS 44
#define N 10*1000*MS
#define D1 100*MS
#define D2 (int)( D1*0.81830988618)
#define D3 (int)( D2*0.81830988618)
#define G 0.7
 
int main(int argc, char **argv)
{
    // Create a 20 ms audio buffer (assuming Fs = 44.1 kHz)
    int16_t buf_in[N] = {0}; // buffer
    int16_t buf_1[N] = {0}; // buffer
    int16_t buf_2[N] = {0}; // buffer
    int16_t buf_3[N] = {0}; // buffer
    int16_t buf_4[N] = {0}; // buffer
    int16_t buf_5[N] = {0}; // buffer
    int16_t buf_r[N] = {0}; // buffer
    int16_t buf_out[N] = {0}; // buffer
    int n;                // buffer index

    // Get input filename from command argument
    char cmd[80] = "ffmpeg -i ";
    strcat(cmd, argv[1]);
    strcat(cmd, " -f s16le -ac 1 -");

    // Open WAV file with FFmpeg and read raw samples via the pipe.
    FILE *pipein;
    pipein = popen(cmd, "r");
    fread(buf_in, 2, N, pipein);
    pclose(pipein);

    for (int t = 0; t < N; t++) {
        if (t < D1) {
            buf_1[t] = buf_in[t];
            buf_out[t] = -buf_in[t];
        } else {
            buf_1[t] = buf_in[t-D1]+G*buf_1[t-D1];
            buf_2[t] = -buf_in[t] + (1-G*G)*buf_1[t];
        }
    }

    for (int t = 0; t < N; t++) {
        if (t < D2) {
            buf_3[t] = buf_2[t];
            buf_out[t] = -buf_3[t];
        } else {
            buf_3[t] = buf_2[t-D2]+G*buf_3[t-D2];
            buf_4[t] = -buf_2[t] + (1-G*G)*buf_3[t];
        }
    }

    for (int t = 0; t < N; t++) {
        if (t < D3) {
            buf_5[t] = buf_4[t];
            buf_out[t] = -buf_5[t];
        } else {
            buf_5[t] = buf_4[t-D3]+G*buf_5[t-D3];
            buf_out[t] = -buf_4[t] + (1-G*G)*buf_5[t];
        }
    }

    // Pipe the audio data to ffmpeg, which writes it to a wav file
    FILE *pipeout;
    pipeout = popen("ffmpeg -y -f s16le -ar 44100 -ac 1 -i - out.wav", "w");
    fwrite(buf_out, 2, N, pipeout);
    pclose(pipeout);

    return 0;
}

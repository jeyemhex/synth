/*======[ CONV_REVERB_T_DOMAIN ]======
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2019-02-12
 *
 * This code is distributed under the MIT license.
 */

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define SAMPLES 44100
#define N (10*SAMPLES)

int main(int argc, char **argv) {
    // Create a 20 ms audio buffer (assuming Fs = 44.1 kHz)
    int16_t buf_in[N]  = {0}; // buffer
    int16_t impulse[N] = {0}; // buffer
    int16_t buf_out[N] = {0}; // buffer

    float float_in[N] = {0.0};
    float float_out[N] = {0.0};
    float float_imp[N] = {0.0};

    // Get input filename from command argument
    char cmd[80] = "ffmpeg -i ";
    strcat(cmd, argv[1]);
    strcat(cmd, " -f s16le -ac 1 -");

    // Open WAV file with FFmpeg and read raw samples via the pipe.
    FILE *signal_in;
    signal_in = popen(cmd, "r");
    fread(buf_in, 2, N, signal_in);
    pclose(signal_in);

    // Get impulse filename from command argument
    strcpy(cmd,"ffmpeg -i ");
    strcat(cmd, argv[2]);
    strcat(cmd, " -f s16le -ac 1 -");

    // Open WAV file with FFmpeg and read raw impulse via the pipe.
    FILE *impulse_in = popen(cmd, "r");
    fread(impulse, 2, N, impulse_in);
    pclose(impulse_in);

    // Convert the input and the impulse to floats
    for (int c = 0; c < N; c++) {
        float_in[c] = buf_in[c];
        float_imp[c] = impulse[c];
    }

    // Convolve the input and impulse signals
    for (int t = 0; t < N; t++) {
        for (int c=0; c < t; c++) {
            float_out[t] += (float_in[c]*float_imp[t-c]/32768);
        }
    }

    // Renormalise the signal and put it into the output buffer
    float max_val = 0;
    for (int c = 1; c < N; c++) {
        max_val = max_val > fabs(float_out[c]) ? max_val : fabs(float_out[c]);
    }
    for (int c = 1; c < N; c++) {
        buf_out[c] = (int) ((float_out[c] / max_val) * 32768);
    }

    // Pipe the audio data to ffmpeg, which writes it to a wav file
    FILE *pipeout;
    pipeout = popen("ffmpeg -y -f s16le -ar 44100 -ac 1 -i - out.wav", "w");
    fwrite(buf_out, 2, N, pipeout);
    pclose(pipeout);

    return 0;
}

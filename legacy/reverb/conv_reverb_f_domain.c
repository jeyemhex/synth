/*======[ CONV_REVERB_F_DOMAIN ]======
 *
 * Author:  Edward Higgins <ed.higgins@york.ac.uk>
 *
 * Version: 0.1.1, 2019-02-12
 *
 * This code is distributed under the MIT license.
 */

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <complex.h>
#include <fftw3.h>

#define SAMPLES 44100
#define N 262144 // needs to be a power of 2
#define PI 3.1415926535

int main(int argc, char **argv) {
    // Create a 20 ms audio buffer (assuming Fs = 44.1 kHz)
    int16_t buf_in[N]  = {0}; // buffer
    int16_t impulse[N] = {0}; // buffer
    int16_t buf_out[N] = {0}; // buffer

    double *time_sig;
    double *time_imp;
    fftw_complex *freq_sig;
    fftw_complex *freq_imp;

    time_sig = malloc(N * sizeof(double));
    time_imp = malloc(N * sizeof(double));
    freq_sig = malloc(N * sizeof(fftw_complex));
    freq_imp = malloc(N * sizeof(fftw_complex));

    // Get input filename from command argument
    char cmd[80] = "ffmpeg -i ";
    strcat(cmd, argv[1]);
    strcat(cmd, " -f s16le -ac 1 -");

    // Open WAV file with FFmpeg and read raw samples via the pipe.
    FILE *signal_in;
    signal_in = popen(cmd, "r");
    fread(buf_in, 2, N, signal_in);
    pclose(signal_in);

    // Get impulse filename from command argument
    strcpy(cmd,"ffmpeg -i ");
    strcat(cmd, argv[2]);
    strcat(cmd, " -f s16le -ac 1 -");

    // Open WAV file with FFmpeg and read the raw impulse via the pipe.
    FILE *impulse_in = popen(cmd, "r");
    fread(impulse, 2, N, impulse_in);
    pclose(impulse_in);

    FILE *impulse_file = fopen("impulse.dat", "w");
    for (int c = 0; c < N; c++) {
        fprintf(impulse_file, "%d\t%d\n", c, impulse[c]);
    }

    // Put the input signal and impulse into complex float arrays
    for (int c = 0; c < N; c++) {
        time_sig[c] = (float) buf_in[c];
        time_imp[c] = (float) impulse[c];
    }

    // Fourier transform the signal and impulse to get them in the frequency domain
    fftw_plan r2c_sig = fftw_plan_dft_r2c_1d(N, time_sig, freq_sig, FFTW_ESTIMATE);
    fftw_plan r2c_imp = fftw_plan_dft_r2c_1d(N, time_imp, freq_imp, FFTW_ESTIMATE);
    fftw_execute(r2c_sig);
    fftw_execute(r2c_imp);
    fftw_destroy_plan(r2c_sig);
    fftw_destroy_plan(r2c_imp);

    // Multiply the signal and impulse frequencies
    for (int c=0; c<N; c+=1) {
        freq_sig[c] = (freq_sig[c]+I*freq_sig[c+1]) * (freq_imp[c]+I*freq_imp[c+1]);
    }

    // Back-transform the signal to the time domain
    fftw_plan c2r_sig = fftw_plan_dft_c2r_1d(N, freq_sig, time_sig, FFTW_ESTIMATE);
    fftw_execute(c2r_sig);
    fftw_destroy_plan(c2r_sig);

    // Renormalise the signal and put it into the output buffer
    float max_val = 0;
    for (int c = 0; c < N; c++) {
        max_val = max_val > fabs(time_sig[c]) ? max_val : fabs(time_sig[c]);
    }
    for (int c = 0; c < N; c++) {
        buf_out[c] = (int16_t) ((time_sig[c] / max_val) * 32768);
    }


    // Pipe the audio data to ffmpeg, which writes it to a wav file
    FILE *pipeout;
    pipeout = popen("ffmpeg -y -f s16le -ar 44100 -ac 1 -i - out.wav", "w");
    fwrite(buf_out, 2, N, pipeout);
    pclose(pipeout);

    return 0;
}

//
// readwav.c - Read samples from a WAV file using FFmpeg via a pipe
// Written by Ted Burke - last updated 10-2-2017
//
// To compile:
//
//    gcc readwav.c -o readwav -lm
//
 
#include <stdio.h>
#include <stdint.h>
 
#define N 88200
#define DELAY 8820
 
int main(int argc, char **argv) {
    // Create a 20 ms audio buffer (assuming Fs = 44.1 kHz)
    int16_t buf_in[N] = {0}; // buffer
    int16_t buf_out[N] = {0}; // buffer
    int n;                // buffer index

    // Get input filename from command argument
    char cmd[80] = "ffmpeg -i ";
    strcat(cmd, argv[1]);
    strcat(cmd, " -f s16le -ac 1 -");

    // Open WAV file with FFmpeg and read raw samples via the pipe.
    FILE *pipein;
    pipein = popen(cmd, "r");
    fread(buf_in, 2, N, pipein);
    pclose(pipein);

    for (int t = 0; t < DELAY; t++) {
        buf_out[t] = buf_in[t];
    }

    for (int t = DELAY; t < N; t++) {
        buf_out[t] = buf_in[t] + buf_out[t-DELAY]*0.7;
    }

    // Pipe the audio data to ffmpeg, which writes it to a wav file
    FILE *pipeout;
    pipeout = popen("ffmpeg -y -f s16le -ar 44100 -ac 1 -i - out.wav", "w");
    fwrite(buf_out, 2, N, pipeout);
    pclose(pipeout);

    return 0;
}

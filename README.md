# SYNTH
## A digital synthesizer in C

## Summary

"Synth" (although that name is probably already taken...) is a software synthesizer
written in C. 

The closest thing to a real synth so far is in `legacy/mono_seq`.
The rest is still early development.

## Compilation

```
    $> make
```

## Dependencies

- PortAudio
- PortMidi (for midi, when I add that)
- NCurses (for the UI, when I add that)

## Known issues

- It's not finished
- ...

## VERSION
Version 0.2.1 (Jan 2019)

## Author
Edward Higgins (ed.j.higgins@gmail.com)
